#include <string>
#include <functional>
#include <iostream>
#include <ranges>
#include <string_view>
#include <algorithm>
#include <numeric>


// Will be used when arbitrary precision integer are needed


class BigInt
{
private:
    std::string digits = "0"; // Contains the value in reverse order to ease computations

    void stripZeros(); // If starting digit is 0, strip it

public:
    BigInt divideBy(const BigInt& rhs, BigInt* remainder = nullptr); // integer division this / rhs


public:
    // Constructor & Destructor
    BigInt() = default;
    BigInt(const unsigned long long int& n);
    BigInt(const BigInt& bigint);
    BigInt(std::string_view str);
    ~BigInt() = default;

    /***************************************************
    ****               HELPER FUNCTIONS             ****
    ***************************************************/

    // Returns digits in correct order
    std::string toString() const;

    // Get digits directly without inversing string
    std::string getDigits() const;

    // Gets the last n digits of the number
    std::string lastDigits( const size_t& n ) const;

    // Get length of digit
    size_t length() const;

    // Returns the sum of the digit 
    int sumDigits() const; 

    // Returns the sum of the digits passed through an additionnal function
    long long int sumDigits( std::function<int(int)> func ) const;

    // Returns (*this)^lhs
    BigInt pow(const BigInt& lhs) const;
    BigInt pow(const long long int& lhs) const;

    // True if number is even
    bool isEven() const;

    bool isPalindrome() const;

    /***************************************************
    ****                OVERLOADING                 ****
    ***************************************************/

    // Copy operator overload
    BigInt& operator=(const BigInt& rhs);

    // Increment operator overload
    BigInt& operator+=(const BigInt& rhs);
    BigInt& operator+=( const unsigned long long int& rhs);

    // Pre-increment and Post-increment operators
    BigInt& operator++();
    BigInt operator++(int);
    
    // Multiplication overloading
    BigInt& operator*=(const BigInt& rhs);
    BigInt& operator*=(const unsigned long long int& rhs);

    // Decrement operators
    BigInt& operator-=(const BigInt& rhs);
    BigInt& operator-=( const unsigned long long int& rhs);
    
    // Pre-decrement and Post-decrement operators
    BigInt& operator--();
    BigInt operator--(int);

    // "Power" operator
    BigInt& operator^=(const BigInt& rhs);

    // Access operator []: Note that it access the num in reverse order
    // i.e. it sample directly this->digits
    char operator[](size_t idx);
    char operator[](size_t idx) const;

};


/***************************************************
****           OVERLOADING DECLARATIONS         ****
***************************************************/


bool operator==(const BigInt& lhs, const BigInt& rhs);
bool operator!=(const BigInt& lhs, const BigInt& rhs);
bool operator==(const BigInt& lhs, const unsigned long long int& rhs);
bool operator!=(const BigInt& lhs, const unsigned long long int& rhs);

// Inequality operators
bool operator<=(const BigInt& lhs, const BigInt& rhs);
bool operator>(const BigInt& lhs, const BigInt& rhs);
bool operator>=(const BigInt& lhs, const BigInt& rhs);
bool operator<(const BigInt& lhs, const BigInt& rhs);

BigInt operator+(const BigInt& lhs, const BigInt& rhs);
BigInt operator+(const BigInt& lhs, const unsigned long long int& rhs);
BigInt operator-(const BigInt& lhs, const BigInt& rhs);
BigInt operator-(const BigInt& lhs, const unsigned long long int& rhs);
BigInt operator*(const BigInt& lhs, const BigInt& rhs);
BigInt operator*(const BigInt& lhs, const unsigned long long int& rhs);

std::ostream& operator<<(std::ostream& os, const BigInt& rhs);








/***************************************************
****************************************************
****    BEGINNING OF FUNCTIONS IMPLEMENTATION   ****
****************************************************
***************************************************/

/***************************************************
****                PRIVATE                     ****
***************************************************/

void BigInt::stripZeros()
{
    while (digits.back() == '0' && digits.size() > 1)
    { digits.pop_back(); }
}

BigInt BigInt::divideBy(const BigInt& rhs, BigInt* out_remainder)
{
    // Some sanity checks
    if ( (*this) < rhs )
    {
        if (out_remainder) *out_remainder = *this;
        return BigInt(0);
    }
    else if ( (*this) == rhs )
    {
        if (out_remainder) *out_remainder = BigInt(0);
        return BigInt(1);
    }

    BigInt res(0);

    while ( ++res * rhs <= (*this) )
    { }
    --res;

    if (out_remainder)
    { *out_remainder = (*this) - ( res * rhs ); }

    return res;
}

/***************************************************
****                 PUBLIC                     ****
***************************************************/

// Constructors and destructors
BigInt::BigInt(const unsigned long long int& n)
    : digits( std::to_string(n) )
{
    std::reverse( digits.begin(), digits.end());
}

BigInt::BigInt(const BigInt& bigint)
    : digits( bigint.digits )
{};

BigInt::BigInt(std::string_view str)
{
    digits.clear();
    // Starts by cleaning up the string
    std::ranges::copy( str
            | std::ranges::views::drop_while([](auto c){return c == '0';}),
            std::back_inserter(digits)
            );

    if (digits.empty())
    {
        digits = "0";
        return;
    }
    if (
            std::find_if(begin(digits), end(digits),
                [](const auto& c){return !std::isdigit(c);})
                != end(digits)
        )
    {
        std::cerr << "Cannot contain other characters than [0-9]\n";
        exit(1);
    }

    std::reverse(begin(digits), end(digits));
}
 
/***************************************************
****             HELPER FUNCTIONS               ****
***************************************************/

// Returns digits in correct order
std::string BigInt::toString() const
{
    std::string val(digits);
    std::reverse(val.begin(), val.end());
    return val;
}

// Get digits directly without inversing string
std::string BigInt::getDigits() const
{ return digits; }

// Gets the last n digits of the number
std::string BigInt::lastDigits( const size_t& n ) const
{
    // Check for out of range
    if ( digits.length() < n )
    {
        std::cerr << "Trying to get " << n << " digits out of a number only containing "
                    << digits.length() << '\n';
        throw( "ERROR" );
    }

    std::string last_digits = digits.substr(0, n ); 
    std::reverse(last_digits.begin(), last_digits.end());
    return last_digits;
}

// Get length of digit
size_t BigInt::length() const
{ return digits.size(); }

// Returns the sum of the digit 
int BigInt::sumDigits() const 
{
    return std::accumulate(begin(digits), end(digits), 0,
            [](auto&& acc, auto& d){ return std::move(acc) + (d - '0'); });
}

// Returns (*this)^lhs
BigInt BigInt::pow(const BigInt& lhs) const
{
    BigInt tmp(*this);
    tmp ^= lhs;
    return tmp;
}
BigInt BigInt::pow(const long long int& lhs) const
{ return this->pow(BigInt(lhs)); }

// True if number is even
bool BigInt::isEven() const
{ return !( (digits[0] - '0') % 2 ); }
 
bool BigInt::isPalindrome() const
{
    auto forward = digits | std::ranges::views::take(digits.size() / 2);
    auto backward = digits | std::ranges::views::reverse | std::ranges::views::take(digits.size() / 2);
    return std::ranges::equal(forward, backward);
}



/***************************************************
****         OVERLOADING IMPLEMENTATION         ****
***************************************************/


// Copy operator overload
BigInt& BigInt::operator=(const BigInt& rhs)
{
    (*this).digits = rhs.digits;
    return *this;
}


// Increment operator overload
BigInt& BigInt::operator+=(const BigInt& rhs)
{
    size_t l_size = digits.length(), r_size = rhs.length();

    // If other num is bigger, add zeros to this.
    if (r_size > l_size) digits.append(r_size - l_size, '0');

    int carry = 0;
    size_t idx = 0;
    for (const auto& d : rhs.digits )
    {
        int result = (d - '0') + (digits[idx] - '0') + carry;
        digits[idx] = (result % 10) + '0';
        carry = result / 10;
        idx++;
    }

    if ( carry == 0 )
    {
        this->stripZeros();
        return *this;
    }

    for (; idx < l_size; ++idx)
    {
        int result = (digits[idx] - '0') + carry;
        digits[idx] = (result % 10) + '0';
        carry = result / 10;
    }

    digits.push_back(carry + '0');
    this->stripZeros();
    return *this;
}

BigInt& BigInt::operator+=( const unsigned long long int& rhs)
{ return (*this)+= BigInt(rhs); }



// Pre-increment and Post-increment operators
BigInt& BigInt::operator++()
{
    (*this) += BigInt(1); 
    return *this;
}
BigInt BigInt::operator++(int)
{
    BigInt tmp(*this);
    ++(*this);
    return tmp;
}



// Multiplication overloading
BigInt& BigInt::operator*=(const BigInt& rhs)
{
    if (this->digits == "0" || rhs.digits == "0")
    {
        this->digits = std::string("0");
        return *this;
    }
    
    size_t len1 = this->length(), len2 = rhs.length();
    std::vector<int> result(len1 + len2, 0);

    // Used to keep track of idices
    size_t idx_1 = 0, idx_2 = 0;

    for (size_t i = 0; i < len1; ++i)
    {
        int carry = 0;
        // Gets digit of num 1
        int digit_1 = (*this)[i] - '0';

        idx_2 = 0;
        for (size_t j = 0; j < len2; ++j)
        {
            // Gets digit of num 2
            int digit_2 = rhs[j] - '0';
            int sum = digit_1 * digit_2 + result[idx_1 + idx_2] + carry;

            carry = sum / 10;
            result[idx_1 + idx_2] = (sum % 10);
            ++idx_2;
        }

        if (carry > 0)
            result[idx_1 + idx_2] += carry;

        ++idx_1;
    }

    std::string res = "";
    for (const auto& d : result)
        res.append( std::to_string(d) );

    digits = res;
    this->stripZeros();
    return *this;
}

BigInt& BigInt::operator*=(const unsigned long long int& rhs)
{ return (*this) *= BigInt(rhs); }


// Decrement operators
BigInt& BigInt::operator-=(const BigInt& rhs)
{
    // Some sanity checks
    if ( *this < rhs )
    {
        std::cerr << "Cannot substract something bigger\n";
        throw( "ERROR" );
    }
    if ( *this == rhs )
    {
        digits = '0';
        return *this;
    }

    int carry = 0;
    // Loop through digits and substract
    for (size_t idx = 0; idx < this->length(); ++idx)
    {
        // Check if we are past the length of right handside number length
        int rhs_digit = ( idx < rhs.length() ) ? (rhs[idx] - '0') : 0;

        int result = ( digits[idx] - '0' ) - rhs_digit - carry;

        if (result < 0)
        {
            result += 10;
            carry = 1;
        }
        else
        {
            carry = 0;
        }

        digits[idx] = result + '0';
    }

    if (carry > 0)
        --digits[rhs.length()];

    this->stripZeros();
    return *this;

}

BigInt& BigInt::operator-=( const unsigned long long int& rhs)
{
    (*this) -= BigInt(rhs);
    return *this;
}


// Pre-decrement and Post-decrement operators
BigInt& BigInt::operator--()
{
    (*this) -= BigInt(1);
    return *this;

}
BigInt BigInt::operator--(int)
{
    BigInt tmp(*this);
    (*this) -= BigInt(1);
    return tmp;
}


// Power operator overload
// Note that we define 0^0 to be 1
BigInt& BigInt::operator^=(const BigInt& rhs)
{
    if ( rhs == 0 )
    {
        digits = '1';
        return *this;
    }

    BigInt counter(rhs), tmp(*this);
    while ( counter > 1 )
    {
        (*this) *= tmp;
        --counter;
    }
    
    return *this;
}


// Acess operators overloading
char BigInt::operator[](size_t idx) {return digits[idx];}
char BigInt::operator[](size_t idx) const { return digits.at(idx);}


/***************************************************
****         OUTSIDE OF CLASS FUNCTIONS         ****
***************************************************/




/***************************************************
****       BOOLEAN OPERATOR OVERLOADING         ****
***************************************************/

// Equality operators
bool operator==(const BigInt& lhs, const BigInt& rhs)
{ return lhs.getDigits() == rhs.getDigits(); }
bool operator!=(const BigInt& lhs, const BigInt& rhs)
{ return !(lhs == rhs); }
bool operator==(const BigInt& lhs, const unsigned long long int& rhs)
{ return lhs == BigInt(rhs); }
bool operator!=(const BigInt& lhs, const unsigned long long int& rhs)
{ return !(lhs == rhs); }

// Inequality operators
bool operator<=(const BigInt& lhs, const BigInt& rhs)
{
    // Some sanity checks
    size_t l_size = lhs.length(), r_size = rhs.length();
    if (l_size != r_size) return (l_size < r_size);

    // Check digit by digit
    for (size_t idx = l_size; idx > 0; --idx)
    {
        if (lhs[idx - 1] != rhs[idx - 1]) { return (lhs[idx - 1] < rhs[idx - 1]); }
    }

    // The two values are equals
    return true;
}
bool operator>(const BigInt& lhs, const BigInt& rhs)
{ return !(lhs <= rhs); }
bool operator>=(const BigInt& lhs, const BigInt& rhs)
{ return (lhs > rhs) || (lhs == rhs);}
bool operator<(const BigInt& lhs, const BigInt& rhs)
{ return (lhs <= rhs) && (lhs != rhs); }


/***************************************************
****     ARITHMETIC OPERATORS OVERLOADING       ****
***************************************************/
BigInt operator+(const BigInt& lhs, const BigInt& rhs)
{
    BigInt tmp(lhs);
    tmp += rhs;
    return tmp;
}
BigInt operator+(const BigInt& lhs, const unsigned long long int& rhs)
{ return lhs + BigInt(rhs); }


BigInt operator-(const BigInt& lhs, const BigInt& rhs)
{
    BigInt tmp(lhs);
    tmp -= rhs;
    return tmp;
}
BigInt operator-(const BigInt& lhs, const unsigned long long int& rhs)
{ return lhs - BigInt(rhs); }


BigInt operator*(const BigInt& lhs, const BigInt& rhs)
{
    BigInt tmp(lhs);
    tmp *= rhs;
    return tmp;
}
BigInt operator*(const BigInt& lhs, const unsigned long long int& rhs)
{ return lhs * BigInt(rhs); }





/***************************************************
****        OTHER OPERATORS OVERLOADING         ****
***************************************************/


// Stream overloading
std::ostream& operator<<(std::ostream& os, const BigInt& rhs)
{ os << rhs.toString(); return os; }
