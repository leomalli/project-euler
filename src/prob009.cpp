#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

#define PRINT_ALL 0

int main(int , char** )
{

    const int target = 1000;
    for (int a = 1; a < target; a++)
    {
        for (int b = 1; b < a; b++)
        {
            int cc = a * a + b * b;
            int c = sqrt(cc);
            if ( (double)c != sqrt(cc) ) continue;

            // Will print all triplet up until we find the right one
            #if PRINT_ALL
            std::cout << a << ' ' << b << ' ' << c << '\n';
            #endif

            if (a + b + c != target) continue;

            std::cout << a << " + " << b << " + " << c << " = " << target << '\n';
            std::cout << "The product abc = " << a * b * c << '\n';
            return 0;
        }
    }


    return 0;
}
