#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

void greatestProd(const std::string& number, const int& n)
{

    long int max = -1;
    auto max_iterator = number.begin();
    for (auto it = number.begin(); it != (number.end() - n + 1); it++)
    {
        long int prod = 1;
        for (auto num_it = it; num_it != (it + n); num_it++)
        {
            prod *= ( *num_it - '0' );
        }
        if (prod > max)
        {
            max = prod;
            max_iterator = it;
        }
    }

    // Print results
    std::cout << *max_iterator;
    for (int i = 1; i < n; i++)
    {
        std::cout << " x " <<  *( max_iterator + i );
    }
    std::cout << " = " << max << '\n';
}

int main(const int argc, const char** argv)
{
    if (argc < 3)
    {
        std::cerr << "Usage: prob8 <adjacents> <bignumber>\n";
        return 1;
    }


    const int n = atoi(argv[1]);
    const std::string number(argv[2]);

    greatestProd( number , n);


    return 0;
}