#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <cmath>
#include <ranges>
#include <numeric>

#include <cassert>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 100       // Default value return by the getArg function
size_t getArg(int argc, char** argv);


            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);
    size_t sum = n * (n+1) / 2;
    size_t sumSqrd = n * (n+1) * (2*n+1) / 6;
    size_t res = std::pow(sum, 2) - sumSqrd;

    std::cout << res << '\n';

    return 0;
}


/* FUNCTION IMPLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
