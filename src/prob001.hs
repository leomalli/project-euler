import Data.List

main :: IO()
main = print $ sum $ multiple_3_or_5 upper

upper :: Integer
upper = 1000

multiple_3_or_5 :: Integer -> [Integer]
multiple_3_or_5 up = [0,3.. up] `union` [0,5.. up]
