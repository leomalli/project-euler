#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <math.h>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 3        // Default value return by the getArg function
size_t getArg(int argc, char** argv);


            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);

    auto is_palindromic = [](auto x){
        std::string num(std::to_string(x));
        auto fwd = num | rv::take(num.size() / 2);
        auto bwd = num | rv::reverse | rv::take(num.size() / 2);
        return std::ranges::equal(fwd, bwd);
    };

    size_t largest{0};
    for (size_t first : rv::iota(1zu,std::pow(10, n)))
    {
        for (size_t second : rv::iota(1zu, first))
        {
            auto num = first * second;
            if (num > largest && is_palindromic(num))
                largest = num;
        }
    }

    std::cout << largest << '\n';

    return 0;
}






/* FUNCTION IMLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
