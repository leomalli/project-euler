main :: IO()
main = print $ (+1) $ argmax $ map collatzLength [1..1000000]

argmax :: [Integer] -> Integer
argmax [] = error "Empty list"
argmax x = snd $ maximum $ zip x (enumFromTo 0 (toInteger $ length x - 1))


collatzNext :: Integer -> Integer
collatzNext n
    | even n = n `div` 2
    | otherwise = 3*n + 1

collatzAccumulate :: Integer -> Integer -> Integer
collatzAccumulate s 1 = s
collatzAccumulate s x = collatzAccumulate (s+1) (collatzNext x)

collatzLength :: Integer -> Integer
collatzLength = collatzAccumulate 1 