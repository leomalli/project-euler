import Language.Haskell.TH (prim)



main :: IO()
main = print $ sum $ takeWhile (<10) primes


factor :: Integer -> Integer -> [Integer]
factor _ 1 = []
factor d n
    | d * d > n = [n]
    | n `mod` d == 0 = d : factor d (n `div` d)
    | otherwise = factor (d+1) n

primeFactors :: Integer -> [Integer]
primeFactors = factor 2

isPrime :: Integer -> Bool
isPrime = null . tail . primeFactors

primes :: [Integer]
primes = filter isPrime [2..]