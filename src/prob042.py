import socket
import requests.packages.urllib3.util.connection as urllib3_cn
 
def allowed_gai_family():
    family = socket.AF_INET    # force IPv4
    return family
 
urllib3_cn.allowed_gai_family = allowed_gai_family
import requests
from math import sqrt

# All that is above is to force IPV4 request to the server

targetURL = "https://projecteuler.net/project/resources/p042_words.txt"

print(f" -- Downloading data from: {targetURL}")
response = requests.get(targetURL)

# Clean data
words = [ word[1:-1] for word in response.text.split(",") ]

# Define triangular number formula
def trigNum(n):
    return n*(n+1) / 2

# Get the word value
def wordValue(str):
    base = ord('A') - 1
    charValues = [ ord(char) - base for char in str ]
    return sum(charValues)

# Lambda to check if num is triangular
# Note: x(x+1)/2 = c => x = (sqrt(8c+1) - 1) / 2 => if x is an
#   integer then c is a triangular number (we don't bother with the
#   negative solution to the above equation)
trig = lambda x : ((sqrt(8*x + 1) - 1) / 2).is_integer()

def isTriangular(str):
    value = wordValue(str)
    return trig(value)

results = [ isTriangular(word) for word in words ]
print(f"We have {sum(results)} triangular numbers in the text file.")