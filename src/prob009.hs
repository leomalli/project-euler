
main :: IO()
main = print $ show $ head correctSum

goalSum :: Int
goalSum = 1000

isSquare :: (Integral a) => a -> Bool
isSquare n = mySqrtRound n ^ 2 == n

mySqrtRound :: (Integral a) => a -> a
mySqrtRound = round . sqrt . fromIntegral

correctSum :: [Int]
correctSum = [ a * b * mySqrtRound (a*a+b*b) | a <- [1..goalSum], b <- [1..a],
    isSquare (a*a + b*b), a + b + mySqrtRound (a*a+b*b) == goalSum]
