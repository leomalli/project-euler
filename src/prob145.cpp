#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <math.h>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 1000        // Default value return by the getArg function
size_t getArg(int argc, char** argv);


            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto upperLimit = getArg(argc, argv);

    /* Defining usefull lambdas */
    /* First reverse is a lot slower but more in the spirit of learning STL algorithms */
    auto reverse = [](auto num)
    {
        std::string str(std::to_string(num));
        std::reverse(str.begin(), str.end());
        size_t result = std::atoll(str.c_str());
        return result;
    };
    auto second_reverse = [](auto num)
    {
        size_t result{0};
        while(num)
        {
            result *= 10;
            result += num % 10;
            num /= 10;
        }
        return result;
    };
    auto hasOddDigits = [](auto num)
    {
        while (num > 0)
        {
            if (num % 2 == 0) return false;
            num /= 10;
        }
        return true;
    };

    /* We actually only need to check odd numbers provided we double the end result */
    auto isOdd = [](auto num)
    {
        return num % 2 == 1;
    };


    /* Create ranges with normal numbers and reversed numbers */
    auto simple_range = rv::iota(1zu, upperLimit + 1) | rv::filter(isOdd);
    auto reversed_range = simple_range | rv::transform(second_reverse);

    /* Save the number who's sum with reversed have only odd digits */
    std::vector<size_t> reversibles;
    for (auto it = reversed_range.begin(); auto i : simple_range)
    {
        auto sum = i + *it;
        if (hasOddDigits(sum))
            reversibles.push_back(i);

        ++it;
    }

    for (auto x : reversibles)
        DEBUG(x);

    std::cout << "Total: " << 2*reversibles.size() << '\n';

    return 0;
}






/* FUNCTION IMLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
