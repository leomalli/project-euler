#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <cmath>
#include <ranges>
#include <numeric>

#include <cassert>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 10'001        // Default value return by the getArg function
size_t getArg(int argc, char** argv);


size_t nthPrime(size_t n)
{
    if (n == 1) return 2;
    if (n == 2) return 3;
    size_t len = n * std::ceil(std::log(std::log(n))) + n * std::ceil(std::log(n));
    len /= 2;
    if (len < 1)
    {
        std::cerr << "Help\n";
        exit(EXIT_FAILURE);
    }
    std::vector<bool> isPrime(len, true);
    isPrime[0] = false;
    for (size_t k = 3; k * k <= len*4 ; k+=2)
    {
        auto idx = k/2;
        if (isPrime[idx])
            for (size_t p = (3*k) / 2; p < len; p+=k)
                isPrime[p] = false;
    }

    for (size_t i{0},counter{1};const auto& b : isPrime)
    {
        i += b;
        counter += 2;
        if (i == n - 1)
            return counter - 2;
    }
    return 0;
}


            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);

    auto res = nthPrime(n);
    std::cout << res << '\n';

    return 0;
}






/* FUNCTION IMPLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
