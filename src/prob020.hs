import Data.Char

main :: IO ()
main = print $ sum . extractDigits $ factorial 100

factorial :: Integer -> Integer
factorial x = product [1 .. x]

extractDigits :: Integer -> [Int]
extractDigits x = map digitToInt $ show x
