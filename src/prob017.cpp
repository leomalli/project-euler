#include <iostream>
#include <utility>
#include <vector>
#include <ranges>
#include <algorithm>
#include <numeric>
#include <string>
#include <math.h>
#include <map>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n'; } while(0)
#endif

const std::map<int, std::string> numbers = {
    {0, ""},
    {1  , "one"},
    {2  , "two"},
    {3  , "three"},
    {4  , "four"},
    {5  , "five"},
    {6  , "six"},
    {7  , "seven"},
    {8  , "eight"},
    {9  , "nine"},
    {10 , "ten"},
    {11 , "eleven"},
    {12 , "twelve"},
    {13 , "thirteen"},
    {14 , "fourteen"},
    {15 , "fifteen"},
    {16 , "sixteen"},
    {17 , "seventeen"},
    {18 , "eighteen"},
    {19 , "nineteen"},
    {20 , "twenty"},
    {30 , "thirty"},
    {40 , "forty"},
    {50 , "fifty"},
    {60 , "sixty"},
    {70 , "seventy"},
    {80 , "eighty"},
    {90 , "ninety"},
    {100, "hundred"},
    {1000, "thousand"}
};

template <class T>
std::tuple<T, T> divide(T num, int mod)
{
    auto remainder = num % mod;
    auto result = num / mod;
    return {result, remainder};
}

std::size_t numLength(int num)
{

    if (num < 20)
        return numbers.at(num).size();
    else if (num < 100)
    {
        auto [tens, ones] = divide(num, 10);
        return numbers.at(tens * 10).size() + numLength(ones);
    }
    else if (num < 1000)
    {
        auto [hundreds, remains] = divide(num, 100);
        constexpr int and_length = 3;
        auto rest = (remains == 0) ? 0 : and_length + numLength(remains);
            return numLength(hundreds) + numbers.at(100).size() + rest;
    }
    else
    {
        auto [thousands, remains] = divide(num, 1000);
        return numLength(thousands) + numbers.at(1000).size() + numLength(remains);
    }
}



int main([[maybe_unused]] const int argc, [[maybe_unused]] const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob017 <upper_bound>\n";
        return 1;
    }

    int n = atoi(argv[1]);


    auto r = rv::iota(1,n + 1);
    auto sum = std::accumulate(r.begin(), r.end(), 0,
            [](auto p, auto x){ return p + numLength(x); } );


    std::cout << sum << '\n';
    return 0;
}
