
main :: IO()
main = print $ sum $ takeWhile ( < upperLimit) [f | f <- fib, even f]

upperLimit = 4_000_000

fib :: [Integer]
fib = 0 : 1 :zipWith (+) fib (tail fib)