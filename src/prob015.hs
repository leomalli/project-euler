main :: IO()
main = print $ 40 `choose` 20


choose :: Integer -> Integer -> Integer
choose _ 0 = 1
choose n k = n * choose (n-1) (k-1) `div` k