#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <execution>
#include <utility>
#include <math.h>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 4'000'000
size_t getArg(int argc, char** argv);

// Creating a Fibonacci iterator class
struct fib_iter
{
    using value_type = size_t;
    using difference_type = std::ptrdiff_t;

    const size_t& operator*() const { return curr_;}
    fib_iter& operator++()
    {
        prev_ = std::exchange(curr_, curr_ + prev_);
        return *this;
    }
    void operator++(int)
    { ++*this;}

private:
    size_t curr_{1}, prev_{0};
};

            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);

    // Creating the fib_view and just summing the values in range
    auto fib_view = std::ranges::subrange<fib_iter, std::unreachable_sentinel_t>{};
    auto is_even = [](auto x){return x % 2 == 0;};
    auto less_than = [n](auto x){return x < n;};
    auto r = fib_view | rv::filter(is_even) | rv::take_while(less_than) | rv::common;
    auto sum = std::reduce(std::execution::par_unseq, begin(r), end(r), 0);

    std::cout << sum << '\n';
    return 0;
}






/* FUNCTION IMLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
