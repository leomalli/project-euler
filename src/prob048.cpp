#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

//#define NDEBUG Define to ignore assertion, stands for NoDEBUG
#include <assert.h>

#include "../include/BigInt.hpp"

int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob048 <n-th power>\n";
        return 1;
    }

    int n = atoi(argv[1]);

    BigInt result;
    for (BigInt i(1); i <= n; ++i)
        result += ( i.pow(i) );

    std::cout << "The lase 10 digits are:\n"
            << result.lastDigits(10) << '\n';

    return 0;
}