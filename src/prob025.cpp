#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

#include <assert.h>

#include "../include/BigInt.hpp"


int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob025 <n-digits>\n";
        return 1;
    }

    size_t n = atoi(argv[1]);

    BigInt N(1), M(1);
    size_t i = 2;
    while (M.length() < n)
    {
        N = std::exchange(M, N + M );
        i++;
    }

    std::cout << "Number: " << i << " : " << M << '\n';

    return 0;
}
