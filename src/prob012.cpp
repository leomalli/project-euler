#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <cmath>
#include <ranges>
#include <numeric>

#include <cassert>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 500        // Default value return by the getArg function
size_t getArg(int argc, char** argv);


            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);

    auto toTriangular = [](auto x){return x*(x+1) / 2;};
    // Note that the function doesn't work for small numbers, but we are not interested in them anyway soooo
    auto numDiv = [](auto x){
        auto sqrt = std::floor(std::sqrt(x));
        auto divs = std::ranges::count_if(rv::iota(1, sqrt), [&x](auto i){
            return x % i == 0;

        });
        return (sqrt * sqrt == x) ? 2*divs + 1 : 2*divs;
    };


    auto r = rv::iota(1) | rv::transform(toTriangular)
                        // | rv::transform(numDiv);
                         | rv::drop_while([&n, &numDiv](auto x){return numDiv(x) < n;});

    for (auto x : r | rv::take(1))
        std::cout << x << '\n';
                           
    return 0;
}






/* FUNCTION IMPLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
