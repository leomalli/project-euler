import qualified Data.Text    as Text
import qualified Data.Text.IO as Text
import Data.Char
import Data.List
import Data.Function

main :: IO ()
main = do
    ls <- fmap Text.lines (Text.readFile "prob099_data.txt")
    -- numbers <- processData
    let processed = map ( map readDecimal . Text.split (==',')) ls
    let powers = map (\x -> log (fromInteger $ head x) * fromInteger (last x)) processed
    print $ snd $ maximum (zip powers [1..])


readDecimal :: Text.Text -> Integer
readDecimal = Text.foldl' step 0
  where step a c = a * 10 + toInteger (digitToInt c)
