import socket
import requests.packages.urllib3.util.connection as urllib3_cn
 
def allowed_gai_family():
    family = socket.AF_INET    # force IPv4
    return family
 
urllib3_cn.allowed_gai_family = allowed_gai_family
import requests

# All that is above is to force IPV4 request to the server

# Dowload target URL
targetURL = "https://projecteuler.net/project/resources/p022_names.txt"
print(f" -- Downloading data from: {targetURL}")
response = requests.get(targetURL)

# Clean data
names = response.text.split(",")
names = [ name[1:-1] for name in names ]
names.sort()

# Get value of a string
def nameValue(str):
    base = ord('A') - 1
    val = [ ord(c) - base for c in str ]
    return sum(val)

# Compute total score
totalScore = 0

# Loop through the file and compute accumulate the score
for (i, name) in enumerate(names):
    totalScore = totalScore + nameValue(name) * (i+1)

print(f"The total score is: {totalScore}")